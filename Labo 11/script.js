function chargerListeCours() {
    var container = document.getElementById("contenu");
    var search = document.getElementById("search").value;
    loadFragment(search)
        .then(content =>
            container.innerHTML = content)
        .catch(error =>
            container.innerHTML = "<p>Erreur de chargement des données</p>");
}

async function loadFragment(search) {
    let response = await fetch("ajax.php?q="+search);
    let content = await response.text();
    return content;
}