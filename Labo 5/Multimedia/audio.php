
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Multimedia</title>
    <meta charset="utf-8">
</head>

<body>
    <!--
        Il faut installer un plugin sur votre navigateur pour jouer un fichier MIDI 
        Par exemple : https://jazz-soft.net/download/midi-player/

    <audio controls autoplay>
        <source src="media/WeAreNumberOne.mid" type="audio/midi">
        Your browser does not support the audio tag.
    </audio> 
    -->
    
    <audio controls autoplay>
        <source src="media/WeAreNumberOne.mp3" type="audio/mp3">
        Your browser does not support the audio tag.
    </audio> 

</body>

</html>