<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        $prenom = $_POST["prenom"];
        $nom = $_POST["nom"];
        $age = $_POST["age"];
        $annee = $_POST["annee"];
        $etablissement = $_POST["etablissement"];
        $programme = $_POST["programme"];
    
        if ($prenom == "") {
            $erreur = $erreur . "Le champ 'Prénom' est obligatoire.<br>";
        }

        if ($nom == "") {
            $erreur = $erreur . "Le champ 'Nom' est obligatoire.<br>";
        }

        if ($age == "") {
            $erreur = $erreur . "Le champ 'Age' est obligatoire.<br>";
        } else if(!is_numeric($age)){
            $erreur = $erreur . "Le champ 'Age' doit contenir uniquement des chiffres.<br>";
        }

        if ($annee == "") {
            $erreur = $erreur . "Le champ 'Année de diplomation' est obligatoire.<br>";
        } else if(!is_numeric($annee)){
            $erreur = $erreur . "Le champ 'Année de diplomation' doit contenir uniquement des chiffres.<br>";
        }

        if ($programme == "") {
            $erreur = $erreur . "Le champ 'Nom du programme' est obligatoire.<br>";
        }

        if ($erreur == "") {
            header("Location: confirmation.php", true, 303);
            exit;
        } 
    }
?>