<?php include "formulaire.php"; ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Formulaires HTML</title>
    <meta charset="utf-8">
</head>

<body>
    <h1>Formulaire d'admission</h1>
    <form action="index.php" method="POST">
        <table>

            <tr>
                <td><label>Nom</label></td>
                <td><input type="text"  maxlength="40" name="nom"></td>
            </tr>

            <tr>
                <td><label>Prénom</label></td>
                <td><input type="text"  maxlength="50" name="prenom"></td>
            </tr>

            <tr>
                <td><label>Âge</label></td>
                <td><input type="text" maxlength="2" name="age"></td>
            </tr>

            <tr>
                <td><label>Année de diplomation</label></td>
                <td><input type="text"  maxlength="4" placeholder="AAAA" name="annee"></td>
            </tr>

            <tr>
                <td><label>Établissement</label></td>
                <td>
                    <select name="etablissement">
                        <option value="uqam">Université du Québec à Montréal</option>
                        <option value="hec">HEC Montréal</option>
                        <option value="laval">Université Laval</option>
                        <option value="teluq">Université TÉLUQ</option>
                        <option value="udeq">Université du Québec</option>
                        <option value="mcgill">Université McGill</option>
                        <option value="udem">Université de Montréal</option>
                        <option value="concordia">Université Concordia</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><label>Nom du programme</label></td>
                <td><input type="text" maxlength="40" name="programme"></td>
            </tr>

            <tr>
                <td>Type d'admission</td>
                <td>
                    <input type="radio" value="Membre régulier" name="membre" checked> Membre régulier<br>
                    <input type="radio" value="Membre or" name="membre">Membre or<br>
                    <input type="radio" value="Membre observateur" name="membre">Membre observateur<br>
                    <input type="radio" value="Membre junior" name="membre">Membre junior<br>
                </td>
            </tr>

            <tr>
                <td>Parcours professionnel</td>
                <td>
                    <textarea rows="4" cols="40" placeholder="Décrivez votre parcours professionnel" name="parcours"></textarea>
                </td>
            </tr>

            <tr>
                <td></td>
                <td><button type="submit">Soumettre</button></td>
            </tr>
        </table>
    </form>
    <?php 
        if ($erreur != "") {
            echo "<p style='color:red'>{$erreur}</p>";
        }
    ?>


</body>

</html>
