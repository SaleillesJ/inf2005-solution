<?php 
echo "<h1>Liste de cours</h1>";

function generateCalendar() {

    echo "<table border='1'>
            <tr>
                <td>Sigle du cours</td>
                <td>Groupe</td>
                <td>Année du cours</td>
                <td>Trimestre</td>
                <td>Titre</td>
            </tr>";

    $nbrCours = 0;

    $file = fopen("data.txt","r"); // Lecture du fichier data.txt
    
    while ((!feof($file)) && ($oneLine = fgets($file))) { // Parcourt toutes les lignes du fichier
        echo "<tr>";

        $annee = substr($oneLine, 0, 4); 

        $trimestre = "Automne";

        if($oneLine[4] == "1"){
            $trimestre = "Hiver";
        } else if($oneLine[4] == "2"){
            $trimestre = "Été";
        }

        $sigle = substr($oneLine, 6, 7);
        $groupe = substr($oneLine, 14, 2);

        $titre = substr($oneLine, 16);
        
        $nbrCours++;

        echo "<td>{$sigle}</td>";
        echo "<td>{$groupe}</td>";
        echo "<td>{$annee}</td>";
        echo "<td>{$trimestre}</td>";
        echo "<td>{$titre}</td>";

        echo "</tr>";
    }
    echo "</table>";
    echo "<h4>Nombre de cours : {$nbrCours}</h4>";
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Introduction au PHP</title>
    </head>
    <body>
        <?php generateCalendar(); ?>
    </body>
</html>



