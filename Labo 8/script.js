var titre = document.getElementById("titre");

function changePolice() {
    var para = document.getElementsByTagName("p");

    for (var i = 0; i < para.length; i++) {
        para[i].style.fontFamily = "Arial, Helvetica, sans-serif";
    }
}

function changeParagraphe() {
    var para1 = document.getElementById("para1").innerHTML;
    var para2 = document.getElementById("para2").innerHTML;
    var para3 = document.getElementById("para3").innerHTML;

    document.getElementById("para1").innerHTML = para2;
    document.getElementById("para2").innerHTML = para3;
    document.getElementById("para3").innerHTML = para1;
}

function changeTitre() {
    var titre = document.getElementById("titre");
    if (titre.style.display == "none") {
        titre.style.display = "block";
    } else {
        titre.style.display = "none";
    }
}

function changeCouleur(){
    document.getElementById("titre").style.color = "red";
}

function addLorem() {
    document.getElementById("para3").outerHTML += "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non elementum magna. Maecenas rutrum convallis ex, vel pretium elit tempus vel.In mi ante, tristique id justo vel, imperdiet elementum urna.Vestibulum porta vel nulla in euismod.</p>";
}