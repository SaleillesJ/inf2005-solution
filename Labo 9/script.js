function valider() {
    if (!(validerDate() && validerLettre("nom") && validerLettre("prenom") && validerContient() && validerGenre() && validerCode())) {
        alert("Fonctionne pas");
        return false;
    }
    return true;
}

function validerDate() {
    var date = new Date(document.getElementById("date").value);

    if (date == "Invalid Date") {
        return false;
    }
    return true;
}

function validerContient() {
    var nom = document.getElementById("nom").value;
    var prenom = document.getElementById("prenom").value;
    var date = document.getElementById("date").value;
    var code = document.getElementById("code").value;

    if (nom == "" || prenom == "" || date == "" || code == "") {
        return false;
    }
    return true;
}

function validerGenre() {
    var genre = document.getElementById("genre").value;
    if (genre != "M" && genre != "F") {
        return false;
    }
    return true;
}

function validerLettre(chaine) {
    var chaine = document.getElementById(chaine).value;

    for (var i = 0; i < chaine.length; i++) {
        if (chaine.length < 3 && (chaine[i] < 'A' || chaine[i] > 'Z') && (chaine[i] < 'a' || chaine[i] > 'z')) {
            return false;
        }
    }
    return true;
}

function validerCode() {
    var nom = document.getElementById("nom").value;
    var prenom = document.getElementById("prenom").value;
    var date = document.getElementById("date").value;
    var genre = document.getElementById("genre").value;
    var code = document.getElementById("code").value;

    if (code.length != 12 || (prenom[0].toUpperCase() != code[3] ||
            nom.substring(0, 3).toUpperCase() != code.substring(0, 3)) ||
        (date.substring(2, 4) != code.substring(8, 10) || date.substring(9, 11) != code.substring(4, 6)) ||
        code[10] < '0' && code[10] > '9' || code[11] < '0' && code[11] > '9') {
        return false;
    } else if (genre == "F") {
        if (parseInt(date.substring(6, 8)) != parseInt(code.substring(6, 8)) - 50) {
            return false;
        }
    } else if (genre == "M") {
        if (date.substring(6, 8) != code.substring(6, 8)) {
            return false;
        }
    }


    return true;
}