<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Ajax</title>
    <meta charset="utf-8">
    <script src="script.js"></script>
</head>

<body>
    <input id="search" type="text" name="q">
    <input id="send" type="submit" value="Charger la liste"><br>
    <div id="contenu">

    </div>

    <script>
    /*
        document.getElementById("send").addEventListener("click", function(){
            var search = document.getElementById("search").value;
            var req = new XMLHttpRequest();
            req.open("GET", "ajax.php?q="+search, true);
            req.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("contenu").innerHTML = this.responseText;
                }
            };
            req.send();
        });
        */

        document.getElementById("send").addEventListener("click", chargerListeCours);
    </script>
</body>
</html>